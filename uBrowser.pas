unit uBrowser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, StdCtrls, ComCtrls, ToolWin, ActiveX,
  ExtCtrls;

type
  TForm_browser = class(TForm)
    WebBrowser1: TWebBrowser;
    ToolBar1: TToolBar;
    ToolSep1: TToolButton;
    ToolRefresh: TToolButton;
    ToolSep2: TToolButton;
    ToolSource: TToolButton;
    Bevel1: TBevel;
    ToolOpenHTML: TToolButton;
    ToolSaveAsHTML: TToolButton;
    Edit1: TEdit;
    ToolButton1: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WebBrowser1NavigateComplete2(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure ToolRefreshClick(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure ToolSourceClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ToolOpenHTMLClick(Sender: TObject);
    procedure ToolSaveAsHTMLClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_browser: TForm_browser;

implementation

uses uMain;

{$R *.dfm}

procedure TForm_browser.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Form_main.ToolBrowser.Down:=false;
end;

procedure TForm_browser.WebBrowser1NavigateComplete2(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
Edit1.Text:=URL;
end;

procedure TForm_browser.ToolRefreshClick(Sender: TObject);
begin
Form_browser.WebBrowser1.Navigate(Edit1.Text);
end;

procedure TForm_browser.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if key=#13 then
  begin
  ToolRefresh.Click;
  end;
end;

procedure TForm_browser.ToolSourceClick(Sender: TObject);
var PersistStream: IPersistStreamInit;
    FileStream: TFileStream;
    Stream: IStream;
    Err:boolean;
    Res:HResult;
begin
if Form_main.IfDocChanged then
  begin
  Err:=false;
  try
    PersistStream := WebBrowser1.Document as IPersistStreamInit;
    FileStream := TFileStream.Create(extractfilepath(paramstr(0))+'HTML2Source.dat', fmCreate);
    Stream := TStreamAdapter.Create(FileStream, soReference) as IStream;
    try
      Res:=PersistStream.Save(Stream, True);
      except Application.MessageBox('������ ��� ������ ����','HTML Editor',mb_iconerror+mb_ok);
             Err:=true;
      end;
    finally
      FileStream.Free;
      if not Err and Not Failed(Res) then Form_main.RichEdit.Lines.LoadFromFile(extractfilepath(paramstr(0))+'HTML2Source.dat');
    end;
  end;
end;

procedure TForm_browser.FormResize(Sender: TObject);
begin
WebBrowser1.Width:=Form_browser.Width-18;
WebBrowser1.Height:=Form_browser.Height-89;
Bevel1.Width:=Form_browser.Width-16;
Bevel1.Height:=Form_browser.Height-87;
Edit1.Width:=Form_browser.Width-195;
end;

procedure TForm_browser.ToolOpenHTMLClick(Sender: TObject);
begin
if Form_main.OpenDlgHTML.Execute then
  begin
  WebBrowser1.Navigate(Form_main.OpenDlgHTML.FileName);
  end;
end;

procedure TForm_browser.ToolSaveAsHTMLClick(Sender: TObject);
var PersistStream: IPersistStreamInit;
    FileStream: TFileStream;
    Stream: IStream;
begin
if form_main.SaveDlgHTML.Execute then
  begin
  try
    PersistStream := WebBrowser1.Document as IPersistStreamInit;
    FileStream := TFileStream.Create(Form_main.SaveDlgHTML.FileName, fmCreate);
    Stream := TStreamAdapter.Create(FileStream, soReference) as IStream;
    try
      PersistStream.Save(Stream, True);
      except Application.MessageBox('������ ��� ���������� ��������','HTML Editor',mb_iconerror+mb_ok);
      end;
    finally
      FileStream.Free;
    end;
  end;
end;

procedure TForm_browser.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case key of
  69: if Shift=[ssCtrl] then ToolSource.Click;
  82: if Shift=[ssCtrl] then ToolRefresh.Click;
  83: if Shift=[ssCtrl,ssShift] then ToolSaveAsHTML.Click;
  87: if Shift=[ssCtrl] then
        begin
        Form_browser.Close;
        Form_main.ToolBrowser.Down:=false;
        end;
  end;
end;

procedure TForm_browser.FormShow(Sender: TObject);
begin
Form_browser.Left:=Form_main.Left+88;
Form_browser.Top:=Form_main.Top+92;
end;

end.
