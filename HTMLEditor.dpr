program HTMLEditor;

uses
  Forms,
  uMain in 'uMain.pas' {Form_main},
  uBrowser in 'uBrowser.pas' {Form_browser},
  uOptions in 'uOptions.pas' {Form_opt},
  uAbout in 'uAbout.pas' {Form_about};

{$R *.res}

begin
  Application.Initialize;
  Application.Title:='HTML Editor';
  Application.CreateForm(TForm_main, Form_main);
  Application.CreateForm(TForm_browser, Form_browser);
  Application.CreateForm(TForm_opt, Form_opt);
  Application.CreateForm(TForm_about, Form_about);
  Application.Run;
end.
