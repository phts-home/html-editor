unit uOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm_opt = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label2: TLabel;
    Edit3: TEdit;
    Label3: TLabel;
    Edit4: TEdit;
    Label4: TLabel;
    Edit5: TEdit;
    Label5: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Edit7: TEdit;
    Edit8: TEdit;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_opt: TForm_opt;

implementation

uses uMain;

{$R *.dfm}

procedure TForm_opt.FormCreate(Sender: TObject);
begin
Edit1.Text:=Form_main.Ini.ReadString('Options','F5','');
Edit2.Text:=Form_main.Ini.ReadString('Options','F6','');
Edit3.Text:=Form_main.Ini.ReadString('Options','F7','');
Edit4.Text:=Form_main.Ini.ReadString('Options','F8','');
Edit5.Text:=Form_main.Ini.ReadString('Options','F9','');
Edit7.Text:=Form_main.Ini.ReadString('Options','F11','');
Edit8.Text:=Form_main.Ini.ReadString('Options','F12','');
end;

procedure TForm_opt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Form_main.Ini.WriteString('Options','F5',Edit1.Text);
Form_main.Ini.WriteString('Options','F6',Edit2.Text);
Form_main.Ini.WriteString('Options','F7',Edit3.Text);
Form_main.Ini.WriteString('Options','F8',Edit4.Text);
Form_main.Ini.WriteString('Options','F9',Edit5.Text);
Form_main.Ini.WriteString('Options','F11',Edit7.Text);
Form_main.Ini.WriteString('Options','F12',Edit8.Text);
end;

procedure TForm_opt.FormShow(Sender: TObject);
begin
Form_opt.Left:=Form_main.Left+98;
Form_opt.Top:=Form_main.Top+148;
end;

end.
