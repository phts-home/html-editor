unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, ExtCtrls;

type
  TForm_about = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Image1: TImage;
    OK: TButton;
    procedure Label5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_about: TForm_about;

implementation

uses uMain;

{$R *.dfm}

procedure TForm_about.Label5Click(Sender: TObject);
begin
ShellExecute(0,'',pchar('mailto:'+Label5.Caption),'','',1);
end;

procedure TForm_about.FormShow(Sender: TObject);
begin
Form_about.Left:=Form_main.Left+93;
Form_about.Top:=Form_main.Top+148;
end;

end.
