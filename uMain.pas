unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, StdCtrls, Buttons, ComCtrls, ToolWin, Menus,
  ExtCtrls, ImgList, IniFiles, XPMan;

type
  TForm_main = class(TForm)
    RichEdit: TRichEdit;
    OpenDlgRich: TOpenDialog;
    SaveDlgRich: TSaveDialog;
    ToolBar1: TToolBar;
    ToolNew: TToolButton;
    ToolSep2: TToolButton;
    ToolOpen: TToolButton;
    ToolSep1: TToolButton;
    ToolSave: TToolButton;
    ToolPrint: TToolButton;
    ToolSep3: TToolButton;
    ToolSep4: TToolButton;
    ToolBrowser: TToolButton;
    ToolOpenHTML: TToolButton;
    OpenDlgHTML: TOpenDialog;
    ToolView: TToolButton;
    SaveDlgHTML: TSaveDialog;
    ImageList2: TImageList;
    ToolSep5: TToolButton;
    ToolOpt: TToolButton;
    ToolSep6: TToolButton;
    ToolAbout: TToolButton;
    XPManifest1: TXPManifest;
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    Panel1: TPanel;
    Label2: TLabel;
    ListBox1: TListBox;
    procedure ToolOpenClick(Sender: TObject);
    procedure ToolNewClick(Sender: TObject);
    procedure ToolSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RichEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolBrowserClick(Sender: TObject);
    procedure ToolOpenHTMLClick(Sender: TObject);
    procedure ToolViewClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolOptClick(Sender: TObject);
    procedure ToolAboutClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RichEditChange(Sender: TObject);
    procedure ToolPrintClick(Sender: TObject);
  private
    { Private declarations }
  public
    Ini:TIniFile;
    Changed:boolean;
    procedure SaveAsHTML (Path:string);
    function IfDocChanged : Boolean;
    Procedure NewDoc;
    { Public declarations }
  end;

var
  Form_main: TForm_main;

implementation

uses uBrowser, uOptions, uAbout;

{$R *.dfm}

procedure TForm_main.ToolOpenClick(Sender: TObject);
begin
if IfDoCChanged then
  if OpenDlgRich.Execute then
    begin
    RichEdit.Clear;
    RichEdit.Lines.LoadFromFile(OpenDlgRich.FileName);
    Changed:=false;
    end;
end;

procedure TForm_main.ToolSaveClick(Sender: TObject);
begin
if SaveDlgRich.Execute then
  begin
  SaveAsHTML(SaveDLGRich.FileName);
  end;
end;

procedure TForm_main.ToolNewClick(Sender: TObject);
begin
if IfDocChanged then NewDoc;
end;

procedure TForm_main.FormCreate(Sender: TObject);
var Rect: TRect;
begin
SendMessage (RichEdit.Handle, EM_GETRECT, 0, LongInt(@Rect));
Rect.Left:= 5;
SendMessage (RichEdit.Handle, EM_SETRECT, 0, LongInt(@Rect));
RichEdit.Refresh;

NewDoc;

Ini:=TIniFile.Create(extractfilepath(paramstr(0))+'HTMLEditor.ini');
Form_main.Left:=Ini.ReadInteger('Program','Left',306);
Form_main.Top:=Ini.ReadInteger('Program','Top',142);
Form_main.Width:=Ini.ReadInteger('Program','Width',528);
Form_main.Height:=Ini.ReadInteger('Program','Height',449);
end;

procedure TForm_main.RichEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=9 then
  begin
  RichEdit.SelText:='  ';
  end;
end;

procedure TForm_main.ToolBrowserClick(Sender: TObject);
begin
if ToolBrowser.Down
  then Form_Browser.Show
  else Form_Browser.Close;
end;

procedure TForm_main.ToolOpenHTMLClick(Sender: TObject);
begin
if OpenDlgHTML.Execute then
  begin
  Form_browser.Show;
  ToolBrowser.Down:=true;
  Form_browser.WebBrowser1.Navigate(OpenDlgHTML.FileName);
  end;
end;

procedure TForm_main.ToolViewClick(Sender: TObject);
var F:textfile;
    i:Integer;
begin
assignfile(F,ExtractFilePath(ParamStr(0))+'Source2HTML.dat');
rewrite(F);
for i:=0 to RichEdit.Lines.Count-1 do
  begin
  writeln(F,RichEdit.Lines[i]);
  end;
closefile(F);
Form_browser.Show;
ToolBrowser.Down:=true;
Form_browser.WebBrowser1.Navigate(ExtractFilePath(ParamStr(0))+'Source2HTML.dat');
end;

procedure TForm_main.FormResize(Sender: TObject);
begin
RichEdit.Width:=Form_main.Width-17;
RichEdit.Height:=Form_main.Height-88;
end;

procedure TForm_main.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case key of
  78: if Shift=[ssCtrl] then ToolNew.Click;   //new
  79: if Shift=[ssCtrl] then ToolOpen.Click;  //open
  83: if Shift=[ssCtrl] then ToolSave.Click;  //save
  80: if Shift=[ssCtrl] then ToolPrint.Click; //print
  87: if Shift=[ssCtrl] then                  //browser
        begin
        if ToolBrowser.Down then
          begin
          Form_Browser.Close;
          ToolBrowser.Down:=false;
          end else
          begin
          Form_Browser.Show;
          ToolBrowser.Down:=true;
          end;
        end;
  81: if Shift=[ssCtrl] then TOOLView.Click;   //��������
  75: if Shift=[ssCtrl] then ToolOpt.Click;    //options
  116:RichEdit.SelText:=Form_opt.Edit1.Text;        //F5
  117:RichEdit.SelText:=Form_opt.Edit2.Text;        //F6
  118:RichEdit.SelText:=Form_opt.Edit3.Text;        //F7
  119:RichEdit.SelText:=Form_opt.Edit4.Text;        //F8
  120:RichEdit.SelText:=Form_opt.Edit5.Text;        //F9
  122:RichEdit.SelText:=Form_opt.Edit7.Text;        //F11
  123:RichEdit.SelText:=Form_opt.Edit8.Text;        //F12
  end;
end;

procedure TForm_main.ToolOptClick(Sender: TObject);
begin
Form_opt.ShowModal;
end;

procedure TForm_main.ToolAboutClick(Sender: TObject);
begin
Form_about.ShowModal;
end;

procedure TForm_main.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Ini.WriteInteger('Program','Left',Form_main.Left);
Ini.WriteInteger('Program','Top',Form_main.Top);
Ini.WriteInteger('Program','Width',Form_main.Width);
Ini.WriteInteger('Program','Height',Form_main.Height);
end;

procedure TForm_main.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
CanClose:=IfDocChanged;
end;

procedure TForm_main.RichEditChange(Sender: TObject);
var x:real;
    xx:integer;
begin
Changed:=true;
x:=length(RichEdit.Text)/1024-trunc(length(RichEdit.Text)/1024);
xx:=trunc(x*100);
if xx<10
  then Label2.Caption:=inttostr(trunc(length(RichEdit.Text)/1024))+'.0'+inttostr(xx)+ ' ��'
  else Label2.Caption:=inttostr(trunc(length(RichEdit.Text)/1024))+'.'+inttostr(xx)+ ' ��';
end;

procedure TForm_main.SaveAsHTML(Path: string);
var H:textfile;
    i:integer;
begin
assignfile(H,Path);
rewrite(H);
for i:=0 to RichEdit.Lines.Count-1 do
  begin
  writeln(H,RichEdit.Lines[i]);
  end;
closefile(H);
Changed:=false;
end;

procedure TForm_main.ToolPrintClick(Sender: TObject);
begin
if PrinterSetupDialog1.Execute then
  if PrintDialog1.Execute
    then Richedit.Print('Ltp1');
end;

function TForm_main.IfDocChanged : Boolean;
var res: HResult;
begin
Result:=false;
if changed then
  begin
  res:=application.MessageBox('�������� ��� �������.'+#13+'�� ������ ��� ���������?','HTML Editor',mb_YESNOCANCEL+MB_ICONQUESTION);
  if res=IDYes then
    begin
    if SaveDlgRich.Execute then
      begin
      SaveAsHTML(SaveDLGRich.FileName);
      Result:=true;
      end;
    end
    else begin
    if res=IDNo
      then Result:=true;
    end;
  end
  else begin
  Result:=true;
  end;
end;

procedure TForm_main.NewDoc;
var H:TextFile;
begin
RichEdit.Clear;
if FileExists(ExtractFilePath(ParamStr(0))+'Template.html')
  then RichEdit.Lines.LoadFromFile(ExtractFilePath(ParamStr(0))+'Template.html')
  else
  begin
  AssignFile(H,ExtractFilePath(ParamStr(0))+'Template.html');
  Rewrite(H);
  CloseFile(H);
  ListBox1.Items.SaveToFile(ExtractFilePath(ParamStr(0))+'Template.html');
  RichEdit.Lines.LoadFromFile(ExtractFilePath(ParamStr(0))+'Template.html');
  end;
if pos('<!--== HTML Editor ==-->',RichEdit.Text)<>0 then
  with Richedit do
    begin
    selstart:=perform (EM_LINEINDEX,6,0);
    end;
Changed:=false;
end;

end.
